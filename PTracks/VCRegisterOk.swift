//
//  VCRegisterOk.swift
//  PTracks
//
//  Created by alexander on 6/25/16.
//  Copyright © 2016 alexander. All rights reserved.
//

import UIKit

class VCRegisterOk: UIViewController {
    
    @IBOutlet weak var RegisterOKBtnvar: UIButton!
    
    var userid:String = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.navigationBar.barTintColor = UIColor.redColor()
        
        self.RegisterOKBtnvar.layer.cornerRadius = 5
        self.RegisterOKBtnvar.layer.masksToBounds = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func MenuButtonTap(sender: UIBarButtonItem) {
        let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.centerContainer!.toggleDrawerSide(MMDrawerSide.Left, animated: true, completion: nil)

    }
    
    @IBAction func RegisterOK(sender: UIButton) {
        let aboutViewController = self.storyboard?.instantiateViewControllerWithIdentifier("VCScan") as! VCScan
        
        aboutViewController.userid = self.userid
        
        let aboutNavController = UINavigationController(rootViewController: aboutViewController)
        
        let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        appDelegate.centerContainer!.centerViewController = aboutNavController
    }

}
