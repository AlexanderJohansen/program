//
//  VCScan.swift
//  PTracks
//
//  Created by alexander on 6/25/16.
//  Copyright © 2016 alexander. All rights reserved.
//

import UIKit

class VCScan: UIViewController {
    
    @IBOutlet weak var ScanBtnvar: UIButton!
    
    var userid:String = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.navigationBar.barTintColor = UIColor.redColor()
        
        self.ScanBtnvar.layer.cornerRadius = 5
        self.ScanBtnvar.layer.masksToBounds = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func MenuButtonTap(sender: UIBarButtonItem) {
        let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.centerContainer!.toggleDrawerSide(MMDrawerSide.Left, animated: true, completion: nil)

    }

    @IBAction func Scan(sender: UIButton) {
        let aboutViewController = self.storyboard?.instantiateViewControllerWithIdentifier("VCQRRecorgnition") as! VCQRRecorgnition
        
        aboutViewController.userid = self.userid
        
        let aboutNavController = UINavigationController(rootViewController: aboutViewController)
        
        let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        appDelegate.centerContainer!.centerViewController = aboutNavController
    }
}
