//
//  VCScanLog.swift
//  PTracks
//
//  Created by alexander on 6/25/16.
//  Copyright © 2016 alexander. All rights reserved.
//

import UIKit
import CoreData

class VCScanLog: UIViewController {
    
    let moc = ScanDataController().managedObjectContext
    
    @IBOutlet weak var ScanBtnvar: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.navigationBar.barTintColor = UIColor.redColor()
        
        self.ScanBtnvar.layer.cornerRadius = 5
        self.ScanBtnvar.layer.masksToBounds = true
        
        self.GetAllData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func MenuButtonTap(sender: UIBarButtonItem) {
        let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.centerContainer!.toggleDrawerSide(MMDrawerSide.Left, animated: true, completion: nil)

    }

    @IBAction func ScanFun(sender: AnyObject) {
        let aboutViewController = self.storyboard?.instantiateViewControllerWithIdentifier("VCScan") as! VCScan
        
        let aboutNavController = UINavigationController(rootViewController: aboutViewController)
        
        let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        appDelegate.centerContainer!.centerViewController = aboutNavController
    }
    
    func GetAllData() -> NSArray{
        let moc = ScanDataController().managedObjectContext
        let scanFetch = NSFetchRequest(entityName: "Scan")
        
        do {
            let fetchScan = try moc.executeFetchRequest(scanFetch) as! [Scan]
            print(fetchScan.last!.locationname!)
            print(fetchScan.last!.currenttime!)
            print(fetchScan.last!.type!)
            
            print(fetchScan.count)
            
            for i in 0...fetchScan.count-1{
                let locationname = fetchScan[i].locationname! as String
                let currenttime = fetchScan[i].currenttime! as String
                let type = fetchScan[i].type! as String
                print(locationname)
                print(currenttime)
                print(type)
            }
            
            
            //            if(fetchPerson.count > 0){
            //                let person = fetchPerson[0] as NSManagedObject
            //
            //                print("1 - \(person)")
            //                if let first = person.valueForKey("firstName"), last = person.valueForKey("lastName"){
            //                    print("\(first)\(last)")
            //                }
            //                print("2 - \(person)")
            //            }
            
        }catch {
            fatalError("bad things happened \(error)")
        }
        return []
    }
    
}
