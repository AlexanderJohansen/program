//
//  VCMenu.swift
//  PTracks
//
//  Created by alexander on 6/24/16.
//  Copyright © 2016 alexander. All rights reserved.
//

import UIKit

class VCMenu: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var menuItems:[String] = ["ScanLog","Profile","FeedBack","Quit"];
    var menuItemsImage:[String] = ["scanlog.png","profile.png","feedback.png","quit.png"];
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = UIColor.grayColor()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return menuItems.count;
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
        
    {
        let mycell = tableView.dequeueReusableCellWithIdentifier("MyCell", forIndexPath: indexPath) as! MyCustomTableViewCell
        mycell.menuItemLabel.text = menuItems[indexPath.row]
        mycell.backgroundColor = UIColor.clearColor()
        mycell.menuItemImage.image = UIImage(named: menuItemsImage[indexPath.row])
        
        return mycell;
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        switch(indexPath.row)
        {
            
        case 0://scan log
            
            let aboutViewController = self.storyboard?.instantiateViewControllerWithIdentifier("VCScanLog") as! VCScanLog
            
            let aboutNavController = UINavigationController(rootViewController: aboutViewController)
            
            let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            
            appDelegate.centerContainer!.centerViewController = aboutNavController
            appDelegate.centerContainer!.toggleDrawerSide(MMDrawerSide.Left, animated: true, completion: nil)
            
            break;
            
        case 1://profile
            
            let centerViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
            
            let centerNavController = UINavigationController(rootViewController: centerViewController)
            
            let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            
            appDelegate.centerContainer!.centerViewController = centerNavController
            appDelegate.centerContainer!.toggleDrawerSide(MMDrawerSide.Left, animated: true, completion: nil)
            
            break;
            
        case 2://Feedback
            
            var inputTextField : String = String()
            
            let alertController = UIAlertController(
                title: "Feedback",
                message: "Please type Feedback, here!",
                preferredStyle: UIAlertControllerStyle.Alert
            )
            
            let cancelAction = UIAlertAction(
                title: "Cancel",
                style: UIAlertActionStyle.Destructive) { (action) in
                    // ...
            }
            
            let confirmAction = UIAlertAction(
            title: "OK", style: UIAlertActionStyle.Default) { (action) in
                // ...
                print("feedback"+inputTextField)
            }
            
            alertController.addAction(confirmAction)
            alertController.addAction(cancelAction)
            
            func configurationTextField(textField: UITextField!)
            {
                textField.text = ""
                inputTextField = textField.text!
            }
            
            alertController.addTextFieldWithConfigurationHandler(configurationTextField)
            presentViewController(alertController, animated: true, completion: nil)
            
            break;
            
        case 3://quit
            
            exit(0)
            
            break;
            
        default:
            
            print("\(menuItems[indexPath.row]) is selected");
            
        }
        
    }
    
}
