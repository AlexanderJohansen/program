//
//  VCQRRecorgnition.swift
//  PTracks
//
//  Created by alexander on 6/25/16.
//  Copyright © 2016 alexander. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation
import CoreData

class VCQRRecorgnition: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet weak var ScanBtnvar: UIButton!
    
    @IBOutlet weak var CameraView: UIView!
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    
    // Added to support different barcodes
    let supportedBarCodes = [AVMetadataObjectTypeQRCode, AVMetadataObjectTypeCode128Code, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeUPCECode, AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeAztecCode]
    
    var userid:String = String()
    var locationid:String = String()
    var location:String = String()
    var visitid:String = String()
    var timestamp:String = String()
    var currenttime:String = String()
    var type:String = String()
    var synctimestamp:String = String()
    var serverid:String = String()
    var visitid_number:Int = Int()
    
    let moc = ScanDataController().managedObjectContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.navigationBar.barTintColor = UIColor.redColor()
        
        self.ScanBtnvar.layer.cornerRadius = 5
        self.ScanBtnvar.layer.masksToBounds = true
        self.CameraView.layer.cornerRadius = 5
        self.CameraView.layer.masksToBounds = true
        
        print("^^^^^^^--- User ID ----^^^^^^^^")
        print(self.userid)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
        // as the media type parameter.
        let captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            // Set the input device on the capture session.
            captureSession?.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
            
            // Detect all the supported bar code
            captureMetadataOutput.metadataObjectTypes = supportedBarCodes
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            videoPreviewLayer?.frame = CameraView.layer.bounds
            CameraView.layer.addSublayer(videoPreviewLayer!)
            
            // Start video capture
            captureSession?.startRunning()
            
            // Move the message label to the top view
//            CameraView.bringSubviewToFront(messageLabel)
            
            // Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            
            if let qrCodeFrameView = qrCodeFrameView {
//                qrCodeFrameView.layer.borderColor = UIColor.greenColor().CGColor
//                qrCodeFrameView.layer.borderWidth = 2
                CameraView.addSubview(qrCodeFrameView)
                CameraView.bringSubviewToFront(qrCodeFrameView)
            }
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRectZero
//            messageLabel.text = "No barcode/QR code is detected"
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        // Here we use filter method to check if the type of metadataObj is supported
        // Instead of hardcoding the AVMetadataObjectTypeQRCode, we check if the type
        // can be found in the array of supported bar codes.
        if supportedBarCodes.contains(metadataObj.type) {
            //        if metadataObj.type == AVMetadataObjectTypeQRCode {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObjectForMetadataObject(metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
//                messageLabel.text = metadataObj.stringValue
                let message = metadataObj.stringValue
                
                //getting current time and tiemstamp
                
                var Timestamp: String {
                    return "\(NSDate().timeIntervalSince1970 * 1000)"
                }
                self.timestamp = Timestamp
                print("++++++timestamp++++++")
                print(self.timestamp)
                
                let date = NSDate();
                let dateFormatter = NSDateFormatter()
                //To prevent displaying either date or time, set the desired style to NoStyle.
                dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle //Set time style
                dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle //Set date style
                dateFormatter.timeZone = NSTimeZone()
                let localDate = dateFormatter.stringFromDate(date)

                print("Local Time")
                print(localDate)
                self.currenttime = localDate
                
                //Analysis of scanning result.
                
                let message_length = message.characters.count as Int
                
                let range = message.rangeOfString("|")
                let index: Int = message.startIndex.distanceTo(range!.startIndex)
                print(index)
                let myNSString = message as NSString
                print(myNSString.substringWithRange(NSRange(location: 0, length: index)))
                self.locationid = myNSString.substringWithRange(NSRange(location: 0, length: index)) as String
                
                let sub_message1 = myNSString.substringWithRange(NSRange(location: index+2, length: message_length-index-2)) as String
                let range1 = sub_message1.rangeOfString("|")
                let index1: Int = sub_message1.startIndex.distanceTo(range1!.startIndex)
                let myNSString1 = sub_message1 as NSString
                let locationname = myNSString1.substringWithRange(NSRange(location: 0, length: index1)) as String
                print(locationname)
                self.location = locationname
                
                let sub_message2 = myNSString1.substringWithRange(NSRange(location: index1+2, length: message_length-index-index1-4)) as String
                let range2 = sub_message2.rangeOfString("|")
                let index2: Int = sub_message2.startIndex.distanceTo(range2!.startIndex)
                let myNSString2 = sub_message2 as NSString
                let unitname = myNSString2.substringWithRange(NSRange(location: 0, length: index2)) as String
                print(unitname)
                
                let sub_message3 = myNSString2.substringWithRange(NSRange(location: index2+2, length: message_length-index-index1-index2-6)) as String
                let range3 = sub_message3.rangeOfString("|")
                let index3: Int = sub_message3.startIndex.distanceTo(range3!.startIndex)
                let myNSString3 = sub_message3 as NSString
                let sitename = myNSString3.substringWithRange(NSRange(location: 0, length: index3)) as String
                print(sitename)
                
                let sub_message4 = myNSString3.substringWithRange(NSRange(location: index3+2, length: message_length-index-index1-index2-index3-8)) as String
                let range4 = sub_message4.rangeOfString("|")
                let index4: Int = sub_message4.startIndex.distanceTo(range4!.startIndex)
                let myNSString4 = sub_message4 as NSString
                let countyname = myNSString4.substringWithRange(NSRange(location: 0, length: index4)) as String
                print(countyname)
                
                let sub_message5 = myNSString4.substringWithRange(NSRange(location: index4+2, length: message_length-index-index1-index2-index3-index4-10)) as String
                let myNSString5 = sub_message5 as String
                let statename = myNSString5
                print(statename)
                
                self.Alert("QRcode Result", message: "You have successfully scanned at \(locationname), \(unitname),\(sitename),\(countyname),\(statename) at \(localDate)")
                self.captureSession?.stopRunning()
            }
        }
    }
    
    func Alert(title:String, message:String){
        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.Alert
        )
        
        let confirmAction = UIAlertAction(
        title: "OK", style: UIAlertActionStyle.Default) { (action) in
            // ...
            print("##### scan result #####")
            print(message)
            
            self.fetch()
            
            self.visitid = String(self.visitid_number + 1)
            
            if Int(self.visitid)! % 2 == 1{
                self.type = "enter"
            }else {
                self.type = "exit"
            }
            
            print("@@@@")
            print(self.visitid)
            print(self.type)
            
            self.seedPerson(self.userid, visitid: self.visitid, locationid: self.locationid, timestamp: self.timestamp, type: self.type, locationname: self.location, currenttime: self.currenttime)
            
            self.fetch()
            
            print("####")
            print(self.visitid)
            print(round(Double(self.timestamp)!))
            print(self.type)
            
            self.showActivityIndicator()
            
            let BaseAPIURL: String = "http://ptracks.com/api/visits/"
            let visits_comment: [String:AnyObject] = [
                "visitid": self.visitid,
                "locationid": self.locationid,
                "timestamp": round(Double(self.timestamp)!),
                "type": self.type
            ]
            let comment: [String:AnyObject] = [
                "userid": self.userid,
                "visits" : visits_comment
            ]
            
            request(.POST, BaseAPIURL, parameters: comment,encoding: .JSON).responseJSON{ result in
                if(result.result.isFailure) {
                    self.hideActivityIndicator()
                    
                    self.Alert_server("Error information", message: "Server is fail response.")

                }
                else {
                    var sJSONObj = JSON(result.result.value!);
                    print("response = \(result.result.value!)")
                    print("-------------")
                    print(sJSONObj)
                    
                    let array = sJSONObj["success"]
                    let subDic = (array.arrayObject)![0]
                    
                    let visitid_net: String = (subDic["visitid"] as? String)!
                    let synctimestamp_net: Double = (subDic["synctimestamp"] as? Double)!
                    let serverid_net: String = (subDic["_id"] as? String)!

                    self.hideActivityIndicator()
                    print("***********-----server response----********")
                    print(visitid_net)
                    print(synctimestamp_net)
                    print(serverid_net)
                    self.Alert_server("Success information", message: "Server is success response.")
                    
                }
            }
            
        }
        
        alertController.addAction(confirmAction)
        
        presentViewController(alertController, animated: true, completion: nil)

    }
    
    func Alert_server(title:String, message:String){
        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.Alert
        )
        
        let confirmAction = UIAlertAction(
        title: "OK", style: UIAlertActionStyle.Default) { (action) in
            // ...
        }
        
        alertController.addAction(confirmAction)
        
        presentViewController(alertController, animated: true, completion: nil)
        
    }

    
    @IBAction func MenuButtonTap(sender: UIBarButtonItem) {
        let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.centerContainer!.toggleDrawerSide(MMDrawerSide.Left, animated: true, completion: nil)

    }
    
    @IBAction func ScanFun(sender: AnyObject) {
        self.captureSession?.startRunning()
    }
    
    
    func seedPerson(userid: String, visitid: String, locationid: String, timestamp: String, type: String, locationname: String, currenttime: String){
        let moc = ScanDataController().managedObjectContext
        
        let entity = NSEntityDescription.insertNewObjectForEntityForName("Scan", inManagedObjectContext: moc) as! Scan
        
        entity.setValue(userid, forKey: "userid")
        entity.setValue(visitid, forKey: "visitid")
        entity.setValue(locationid, forKey: "locationid")
        entity.setValue(timestamp, forKey: "timestamp")
        entity.setValue(type, forKey: "type")
        entity.setValue(locationname, forKey: "locationname")
        entity.setValue(currenttime, forKey: "currenttime")
        
        do{
            try moc.save()
        }catch {
            fatalError("failure to save context: \(error)")
        }
    }

    func fetch() {
        let moc = ScanDataController().managedObjectContext
        let scanFetch = NSFetchRequest(entityName: "Scan")
        
        do {
            let fetchScan = try moc.executeFetchRequest(scanFetch) as! [Scan]
            if fetchScan.count > 0 {
                print("------------read scan result--------")
                print(fetchScan.last!.userid!)
                print(fetchScan.last!.locationid!)
                print(fetchScan.last!.visitid!)
                print(fetchScan.last!.timestamp!)
                print(fetchScan.last!.type!)
                
                self.visitid = fetchScan.last!.visitid!
                self.visitid_number = Int(self.visitid)!
            } else {
                self.visitid_number = 0
            }
        }catch {
            fatalError("bad things happened \(error)")
        }
    }
    
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    /*
     Show customized activity indicator,
     actually add activity indicator to passing view
     
     @param uiView - add activity indicator to this view
     */
    func showActivityIndicator() {
        let uiView = self.view
        
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColor.whiteColor()
        
        loadingView.frame = CGRectMake(0, 0, 80, 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor.lightGrayColor()
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        activityIndicator.center = CGPointMake(loadingView.frame.size.width / 2, loadingView.frame.size.height / 2);
        
        loadingView.addSubview(activityIndicator)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        activityIndicator.startAnimating()
    }
    /*
     Hide activity indicator
     Actually remove activity indicator from its super view
     
     @param uiView - remove activity indicator from this view
     */
    func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
    }

}
