//
//  ViewController.swift
//  PTracks
//
//  Created by alexander on 6/24/16.
//  Copyright © 2016 alexander. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {

    var kbHeight : CGFloat!
    
    @IBOutlet weak var RegisterBtnvar: UIButton!
    
    @IBOutlet weak var FirstName: UITextField!
    @IBOutlet weak var LastName: UITextField!
    @IBOutlet weak var Company: UITextField!
    @IBOutlet weak var JobTitle: UITextField!
    @IBOutlet weak var CardID: UITextField!
    @IBOutlet weak var EmploymentStatus: UILabel!
    
    
    @IBOutlet weak var pickerButton: UIButton!
    @IBOutlet weak var pickerViewController: UIView!
    @IBOutlet weak var databasePickerview: UIPickerView!
    @IBOutlet weak var pickerTextField: UILabel!
    
    var databaseArray = [String]()
    var beforeString = String()
    var afterString = String()
    
    let moc = DataController().managedObjectContext
    
    var userid:String = String()
    var isactive:NSNumber = NSNumber()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.navigationBar.barTintColor = UIColor.redColor()
        
//        self.CellButton.setTitle("Go to Lesson", forState: .Normal)   //button's title
        self.RegisterBtnvar.layer.cornerRadius = 5
        self.RegisterBtnvar.layer.masksToBounds = true
        
        self.FirstName.delegate = self
        self.LastName.delegate = self
        self.Company.delegate = self
        self.JobTitle.delegate = self
        self.CardID.delegate = self
        
        pickerViewController.hidden = true
        pickerViewController.backgroundColor = UIColor(red: 204/256, green: 204/256, blue: 204/256, alpha: 1)
        databaseArray = ["BHP Employee","BHP Agency Contractor","Primary Contractor","Third Contractor","Sub Contractor"]
        beforeString = "BHP Employee"
        pickerTextField.text = beforeString
        pickerTextField.backgroundColor = UIColor(red: 90/256, green: 255/256, blue: 167/256, alpha: 1)
        
        self.fetch()
        if self.isactive == 1 {
            let aboutViewController = self.storyboard?.instantiateViewControllerWithIdentifier("VCScan") as! VCScan
            
            aboutViewController.userid = self.userid
            
            let aboutNavController = UINavigationController(rootViewController: aboutViewController)
            let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = aboutNavController
        }
        
    }
    
    @IBAction func pickerBtnAction(sender: UIButton) {
        pickerViewController.hidden = false
        databasePickerview.reloadAllComponents()
    }
    
    @IBAction func pickerViewActionDone(sender: UIButton) {
        pickerViewController.hidden = true
    }
    
    @IBAction func pickerViewActionCancel(sender: UIButton) {
        pickerViewController.hidden = true
        pickerTextField.text = beforeString
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return databaseArray.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return databaseArray[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        beforeString = pickerTextField.text!
        pickerTextField.text = databaseArray[row]
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    func keyboardWillShow(notification:NSNotification){
        
        self.RegisterBtnvar.enabled = false
        if let userInfo = notification.userInfo{
            if let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue(){
                kbHeight = keyboardSize.height/2
                UIView.animateWithDuration(0.3, animations: {
                    self.view.frame = CGRectOffset(self.view.frame, 0, -self.kbHeight)})
                NSNotificationCenter.defaultCenter().removeObserver(self)
                NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
            }
        }
        
    }
    
    func keyboardWillHide(notification:NSNotification){
        self.RegisterBtnvar.enabled = true
        if let userInfo = notification.userInfo{
            if let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue(){
                kbHeight = keyboardSize.height/2
                UIView.animateWithDuration(0.3, animations: {
                    self.view.frame = CGRectOffset(self.view.frame, 0, self.kbHeight)})
                NSNotificationCenter.defaultCenter().removeObserver(self)
                NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
            }
        }
        
    }

    //when textfield got focus
    func textFieldDidBeginEditing(textField: UITextField) {
        print(textField.tag)
        if textField.tag == 4 || textField.tag == 5 {
            print("dodobal solution")
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        }else {
            NSNotificationCenter.defaultCenter().removeObserver(self)
        }
    }
    
    //when textField lost focus
    func textFieldDidEndEditing(textField: UITextField) {
        
    }
    
//    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
//        self.view.endEditing(true)
//    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if (textField == self.FirstName) {
            self.LastName.becomeFirstResponder()
        }
        else if (textField == self.LastName) {
            self.Company.becomeFirstResponder()
            
        } else if (textField == self.Company) {
            self.JobTitle.becomeFirstResponder()
        }
        else if (textField == self.JobTitle) {
            self.CardID.becomeFirstResponder()
        }
        else{
            let thereWereErrors = checkForErrors()
            if !thereWereErrors
            {
                //conditionally segue to next screen
            }
        }
        
        return true
    }
    
    func checkForErrors() -> Bool
    {
        var errors = false
        let title = "Error"
        var message = ""
        if FirstName.text!.isEmpty {
            errors = true
            message += "First name empty"
            alertWithTitle(title, message: message, ViewController: self, toFocus:self.FirstName)
            
        }
        else if LastName.text!.isEmpty
        {
            errors = true
            message += "Last name empty"
            alertWithTitle(title, message: message, ViewController: self, toFocus:self.LastName)
            
            self.LastName.becomeFirstResponder()
        }
        else if Company.text!.isEmpty
        {
            errors = true
            message += "Company empty"
            alertWithTitle(title, message: message, ViewController: self, toFocus:self.Company)
            
        }
        else if JobTitle.text!.isEmpty
        {
            errors = true
            message += "JobTitle empty"
            alertWithTitle(title, message: message, ViewController: self, toFocus:self.JobTitle)
            
        }
        else if CardID.text!.isEmpty
        {
            errors = true
            message += "CardID empty"
            alertWithTitle(title, message: message, ViewController: self, toFocus:CardID)
        }
        
        return errors
    }
    
    func alertWithTitle(title: String!, message: String, ViewController: UIViewController, toFocus:UITextField) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,handler: {_ in
            toFocus.becomeFirstResponder()
        });
        alert.addAction(action)
        ViewController.presentViewController(alert, animated: true, completion:nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func MenuButtonTap(sender: UIBarButtonItem) {
        let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.centerContainer!.toggleDrawerSide(MMDrawerSide.Left, animated: true, completion: nil)
    }

    @IBAction func RegisterBtn(sender: UIButton) {
        
        if FirstName.text!.isEmpty {
            self.Alert("Warning", message: "Please input your FirstName")
        }
        else if LastName.text!.isEmpty
        {
            self.Alert("Warning", message: "Please input your LastName")
        }
        else if Company.text!.isEmpty
        {
            self.Alert("Warning", message: "Please input your Company")
        }
        else if JobTitle.text!.isEmpty
        {
            self.Alert("Warning", message: "Please input your JobTitle")
        }
        else if CardID.text!.isEmpty
        {
            self.Alert("Warning", message: "Please input your CardID")
        }
        else{
            self.showActivityIndicator()
            
            var firstname = ""
            var lastname = ""
            var company = ""
            var cardid = ""
            var estatus = ""
            var jobtitle = ""
            var userid = ""
            var isactive = false
            
            let BaseAPIURL: String = "http://ptracks.com/api/users/"
            let payload_comment: [String:AnyObject] = [
                "os": "ios",
                "version": "9.3"
            ]
            let device_comment: [String:AnyObject] = [
                "id": "123",
                "payload": payload_comment
            ]
            let comment: [String:AnyObject] = [
                "firstname": self.FirstName.text!,
                "lastname": self.LastName.text!,
                "company": self.Company.text!,
                "cardid": self.CardID.text!,
                "estatus": self.EmploymentStatus.text!,
                "device" : device_comment,
                "jobtitle" : self.JobTitle.text!
            ]
            
            request(.POST, BaseAPIURL, parameters: comment,encoding: .JSON).responseJSON{ result in
                if(result.result.isFailure) {
                    self.Alert("Error", message: result.result.error!.description);
                }
                else {
                    var sJSONObj = JSON(result.result.value!);
                    print("response = \(result.result.value!)")
                    print("-------------")
                    print(sJSONObj)
                    
                    firstname = sJSONObj["firstname"].string!
                    lastname = sJSONObj["lastname"].string!
                    company = sJSONObj["company"].string!
                    cardid = sJSONObj["cardid"].string!
                    estatus = sJSONObj["estatus"].string!
                    jobtitle = sJSONObj["jobtitle"].string!
                    userid = sJSONObj["_id"].string!
                    isactive = sJSONObj["isactive"].bool!
                    
                    self.seedPerson(firstname, lastname: lastname, company: company, cardid: cardid, estatus: estatus, jobtitle: jobtitle, userid: userid, isactive: isactive)
                    
                    if isactive == true {
                        
                        self.hideActivityIndicator()
                        print("***********---------********")
                        self.fetch()
                        
                        let aboutViewController = self.storyboard?.instantiateViewControllerWithIdentifier("VCRegisterOk") as! VCRegisterOk
                        aboutViewController.userid = self.userid
                        let aboutNavController = UINavigationController(rootViewController: aboutViewController)
                        let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                        appDelegate.centerContainer!.centerViewController = aboutNavController
                    }
                    else {
                        
                        self.hideActivityIndicator()
                        
                        self.Alert("Error information", message: "current user is not active.")
                    }
                }
            }
        }
        
    }
    
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    /*
     Show customized activity indicator,
     actually add activity indicator to passing view
     
     @param uiView - add activity indicator to this view
     */
    func showActivityIndicator() {
        let uiView = self.view
        
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColor.whiteColor()
        
        loadingView.frame = CGRectMake(0, 0, 80, 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor.lightGrayColor()
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        activityIndicator.center = CGPointMake(loadingView.frame.size.width / 2, loadingView.frame.size.height / 2);
        
        loadingView.addSubview(activityIndicator)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        activityIndicator.startAnimating()
    }
    /*
     Hide activity indicator
     Actually remove activity indicator from its super view
     
     @param uiView - remove activity indicator from this view
     */
    func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
    }
    
    func Alert(title:String, message:String){
        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.Alert
        )
        
        let confirmAction = UIAlertAction(
        title: "OK", style: UIAlertActionStyle.Default) { (action) in
            // ...
        }
        
        alertController.addAction(confirmAction)
        
        presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    func seedPerson(firstname: String, lastname: String, company: String, cardid: String, estatus: String, jobtitle: String, userid: String, isactive: Bool){
        let moc = DataController().managedObjectContext
        
        let entity = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: moc) as! User
        
            entity.setValue(firstname, forKey: "firstname")
            entity.setValue(lastname, forKey: "lastname")
            entity.setValue(company, forKey: "company")
            entity.setValue(cardid, forKey: "cardid")
            entity.setValue(estatus, forKey: "estatus")
            entity.setValue(jobtitle, forKey: "jobtitle")
            entity.setValue(userid, forKey: "userid")
            entity.setValue(isactive, forKey: "isactive")
        
        do{
            try moc.save()
        }catch {
            fatalError("failure to save context: \(error)")
        }
    }
    
    func fetch() {
        let moc = DataController().managedObjectContext
        let personFetch = NSFetchRequest(entityName: "User")
        
        do {
            let fetchPerson = try moc.executeFetchRequest(personFetch) as! [User]
            if fetchPerson.count > 0 {
                print(fetchPerson.last!.firstname!)
                print(fetchPerson.last!.lastname!)
                print(fetchPerson.last!.company!)
                print(fetchPerson.last!.cardid!)
                print(fetchPerson.last!.estatus!)
                print(fetchPerson.last!.jobtitle!)
                print(fetchPerson.last!.userid!)
                print(fetchPerson.last!.isactive!)
                
                self.userid = fetchPerson.last!.userid!
                self.isactive = fetchPerson.last!.isactive!
            } else {
                self.isactive = 0
            }
        }catch {
            fatalError("bad things happened \(error)")
        }
    }
}

