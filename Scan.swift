//
//  Scan.swift
//  PTracks
//
//  Created by alexander on 6/28/16.
//  Copyright © 2016 alexander. All rights reserved.
//

import UIKit
import CoreData

class Scan: NSManagedObject {
    @NSManaged var userid: String?
    @NSManaged var visitid: String?
    @NSManaged var locationid: String?
    @NSManaged var timestamp: String?
    @NSManaged var type: String?
    @NSManaged var locationname: String?
    @NSManaged var currenttime: String?
}
