//
//  User.swift
//  PTracks
//
//  Created by alexander on 6/28/16.
//  Copyright © 2016 alexander. All rights reserved.
//

import Foundation
import CoreData


class User: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    @NSManaged var firstname: String?
    @NSManaged var lastname: String?
    @NSManaged var company: String?
    @NSManaged var cardid: String?
    @NSManaged var estatus: String?
    @NSManaged var jobtitle: String?
    @NSManaged var userid: String?
    @NSManaged var isactive: NSNumber?
}
